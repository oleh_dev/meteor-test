import { Meteor } from 'meteor/meteor';
import crypto from 'crypto';

const host = "http://shopify.nodex.in.ua:8880";
const sharedSecret = "20eee336d02ede62114bf66fb1275b27";

Meteor.startup(() => {

  Shopify.addKeyset("default", {
  	api_key: "5fc44cde59b303f2595ea705c730d3f0",
  	password: "6221769488e5807ea402295c89540c01"
  });

});

Router.route('/register_webhook', function () {

	try {
		var api = new Shopify.API({
			shop: "oleh-4",
			keyset: "default"
		});

		api.createWebhook({data: {
			webhook: {
				"topic": "customers/update",
				"address": host + "/webhook/customer/update",
				"format": "json"
			}
		}});
		this.response.end("Webhook created");
	} catch (e) {
		this.response.writeHead(400);
		this.response.end(e.message);
	}


}, {where: 'server'});

Router.route('/webhook/customer/update', function () {

	// just ok response, to say shopify that we receive webhook
	this.response.end("ok");

	var request = this.request;

	// seems JSON.stringify(request.body) is not a good way, needs to get raw post data
	const hash = crypto.createHmac('sha256', sharedSecret).update(JSON.stringify(request.body)).digest('base64');

	// request verified
	if (hash == request.headers['x-shopify-hmac-sha256']){
		let updatedCustomer = request.body;

		var api = new Shopify.API({
			shop: "oleh-4",
			keyset: "default"
		});

		var customerSavedSearches = api.getCustomerSavedSearches({fields: "id,name"});

		let tagsArray = [];
		let tags = "";

		let customersFromCustomerSavedSearch = [];

		// get customers for each saved search
		customerSavedSearches.forEach(savedSearch => {

			customersFromCustomerSavedSearch = api.getCustomersFromCustomerSavedSearch({id: savedSearch.id, fields: "id"});

		  // check each found customer, if he is updated customer
		  customersFromCustomerSavedSearch.forEach(cutomer => {

   			// if so, we store saved search name as tag
	  		if (cutomer.id == updatedCustomer.id){
			    tagsArray.push(savedSearch.name);
		    }

	    });

		// Alternative to use searchCustomers instead getCustomersFromCustomerSavedSearch

	  });

		// do not remove manual tags
		if (updatedCustomer.tags.length > 0)
			tagsArray = _.union( updatedCustomer.tags.split(', '), tagsArray);

		if (tagsArray.length > 0)
			tags = tagsArray.join(', ');

		// do not update customers if tags are same
		// better way to compare arrays
		if (updatedCustomer.tags !== tags) {

			// set tags for customer
			api.modifyCustomer({
				id: updatedCustomer.id, data: {
					customer: {
						id: updatedCustomer.id,
						tags: tags
					}
				}
			});

		}

	}

}, {where: 'server'});